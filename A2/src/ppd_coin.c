 /***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #2
 * Full Name        : Rei Ito
 * Student Number   : s3607050
 * Course Code      : COSC1076
 * Program Code     : BP215
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/
#include "ppd_coin.h"

 /**
  * @file ppd_coin.c implement functions here for managing coins in the
  * "cash register" contained in the @ref ppd_system struct.
  **/

/* validate denominations */
BOOLEAN validDenom(char *denom)
{
   if (strlen(denom) > DENOM_LEN)
   {
      printf("[INVALID] Denomination length is too long. \n");
      return FALSE;
   }
   
   if (strcmp(denom, "5") > 0 && strcmp(denom, "1000") < 0)
   {
      printf("[INVALID] Denomination is too small/big. \n");
      return FALSE;
   }
   
   return TRUE;
}

/* validate quantity */
BOOLEAN validQty(char *qty)
{
   if (strcmp(qty, "") == 0)
   {
      printf("[INVALID] Quantity is missing. \n");
      return FALSE;
   }
   
   if (strlen(qty) > QTY_LEN)
   {
      printf("[INVALID] Quantity length is too long. \n");
      return FALSE;
   }
   
   if (strcmp(qty, "0") < 0 || strcmp(qty, "99") > 0)
   {
      printf("[INVALID] Quitity is too small/big. \n");
      return FALSE;
   }
   
   return TRUE;
}