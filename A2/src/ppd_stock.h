 /***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #2
 * Full Name        : Rei Ito
 * Student Number   : s3607050
 * Course Code      : COSC1076
 * Program Code     : BP215
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <limits.h>
#include <ctype.h>
#include "ppd_coin.h"
#include "ppd_shared.h"

#ifndef PPD_STOCK
#define PPD_STOCK

/**
 * @file ppd_stock.h this file defines the data structures required to 
 * manage the stock list. You should add here the function prototypes for
 * managing the list here and implement them in ppd_stock.c
 **/

#define DELIMETER 1
 
/**
 * The length of the id string not counting the nul terminator
 **/
#define ID_LEN 5

/**
 * The maximum length of a product name not counting the nul terminator
 **/
#define NAME_LEN 40

/**
 * The maximum length of a product description not counting the nul
 * terminator.
 **/
#define DESC_LEN 255

#define CURRENCY_LEN 2

#define ON_HAND_LEN 2

#define STOCK_LINE_LEN ID_LEN + NAME_LEN + DESC_LEN + CURRENCY_LEN + ON_HAND_LEN + (DELIMETER*4)

/**
 * The default coin level to reset the coins to on request
 **/
#define DEFAULT_COIN_COUNT 20

/**
 * The default stock level that all new stock should start at and that 
 * we should reset to on restock
 **/
#define DEFAULT_STOCK_LEVEL 20

/**
 * The number of denominations of currency available in the system 
 **/
#define NUM_DENOMS 8

#define MAX_DOLLARS 9
#define MAX_CENTS 95
#define MAX_ON_HAND 99

/**
 * a structure to represent a price. One of the problems with the floating
 * point formats in C like float and double is that they have minor issues
 * of inaccuracy due to rounding. In the case of currency this really is
 * not acceptable so we introduce our own type to keep track of currency.
 **/
struct price
{
   /*the dollar and cents value for some price*/
   unsigned dollars, cents;
};

/**
 * data structure to represent a stock item within the system
 **/
typedef struct ppd_stock
{
    /* the unique id for this item */
    char id[ID_LEN + 1];
    /* the name of this item */
    char name[NAME_LEN + 1];
    /* the description of this item */
    char desc[DESC_LEN + 1];
    /* the price of this item */
    struct price price;
    /* the quantity of this item */
    unsigned on_hand;
    /**
     * a pointer to the next node in the list
     **/
} Item;

/**
 * the node that holds the data about an item stored in memory
 **/
typedef struct ppd_node
{
   /* pointer to the data held for the node */
   Item *data;
   /* pointer to the next node in the list */
   struct ppd_node *next;
} Node;

/**
 * the list of products - each link is the list is a @ref ppd_node
 **/
typedef struct ppd_list
{
   /* the beginning of the list */
   Node *head;
   /* the amount of nodes in the list */
   unsigned count;
} StockList;

/**
 * this is the header structure for all the datatypes that is 
 * passed around and manipulated
 **/
typedef struct ppd_system
{
   /* the container for all the money manipulated by the system */
   struct coin cash_register[NUM_DENOMS];

   /* the linked list */
   StockList *item_list;

   /**
    * the name of the coin file - we need this for saving as all menu
    * items only take the one parameter of the ppd_system
    **/
   const char *coin_file_name;
   /* the name of the stock file */
   const char *stock_file_name;

   /* are the coins loaded in from a file ? */
   BOOLEAN coin_from_file;
} System;

/* function declarations */
BOOLEAN addToStockList(System *, Item *);
BOOLEAN removeFromStockList(System *, char *);
BOOLEAN idExists(System *, char *);
BOOLEAN validID(char *);
BOOLEAN validName(char *);
BOOLEAN validDesc(char *);
BOOLEAN validPrice(struct price);
BOOLEAN validOnHand(char *);
#endif