 /***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #2
 * Full Name        : Rei Ito
 * Student Number   : s3607050
 * Course Code      : COSC1076
 * Program Code     : BP215
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/

#include "ppd_options.h"

/**
 * @file ppd_options.c this is where you need to implement the main 
 * options for your program. You may however have the actual work done
 * in functions defined elsewhere. 
 * @note if there is an error you should handle it within the function
 * and not simply return FALSE unless it is a fatal error for the 
 * task at hand. You want people to use your software, afterall, and
 * badly behaving software doesn't get used.
 **/

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true as this particular function should never fail.
 **/
BOOLEAN display_items(System *system)
{
   int i;
   Node *currentNode = system->item_list->head;
   
   printf("\nItems Menu \n\n");
   printf("%-*s | %-*s | %-*s | %-*s \n", ID_COL_LEN, "ID", NAME_COL_LEN(system), "Name", AVAIL_COL_LEN, "Available", PRICE_COL_LEN, "Price");
   for (i = 0; i < (SPACES * SPACING_LEN) + ID_COL_LEN + NAME_COL_LEN(system) + AVAIL_COL_LEN + PRICE_COL_LEN; i++)
   {
      printf("%s", "-");
   }
   
   printf("\n");
   
   while (currentNode != NULL)
   {
      printItem(system, currentNode->data);

      currentNode = currentNode->next;
   }
   
   printf("\n");
   
   return TRUE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true when a purchase succeeds and false when it does not
 **/
BOOLEAN purchase_item(System *system)
{
   Item *selectedItem = NULL;
   char *idPrompt = "Please enter the ID of the item you wish to purchase: ";
   char priceStr[CURRENCY_LEN + 1];
   char changeStr[CURRENCY_LEN + 1];
   int itemPrice, insertValue, change;
   struct price *amtLeft, *changePrice;
   BOOLEAN complete = FALSE;
   
   printf("\nPurchase Item \n\n");
   
   selectedItem = stockSearch(idPrompt, system);
   
   if (selectedItem == NULL)
   {
      return TRUE;
   }
   if (selectedItem->on_hand == 0)
   {
      printf("[ERROR] The item selected is out of stock, please try again when it's restocked \n");
      return FALSE;
   }
   
   priceToStr(priceStr, selectedItem->price);
   
   printf("You have selected: %s, %s. This will cost you %s. \n", selectedItem->name, selectedItem->desc, priceStr);
   printf("Please hand over the money – type in the value of each note/coin in cents. \n");
   printf("(Press enter on a new and empty line to cancel this purchase) \n");
   
   itemPrice = (selectedItem->price.dollars * 100) + selectedItem->price.cents;
   amtLeft = &selectedItem->price;
   changePrice = &selectedItem->price;
   
   while (!complete)
   {
      priceToStr(priceStr, *amtLeft);
      printf("You still need to give us %s: ", priceStr);
      
      insertValue = validInsertAmt(5, 1000);
      
      if (insertValue >= itemPrice)
      {
         change = insertValue - itemPrice;
         inpToPrice(changePrice, change);
         priceToStr(changeStr, *changePrice);
         complete = TRUE;
      }
      else
      {
         itemPrice -= insertValue;
      }
      
      inpToPrice(amtLeft, itemPrice);
   }
   
   selectedItem->on_hand--;
   
   printf("Thank you. Here is your %s, and your change of %s. \n", selectedItem->name, changeStr);
   printf("Please come back soon. \n\n");
   
   return TRUE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true when a save succeeds and false when it does not
 **/
BOOLEAN save_system(System *system)
{
   FILE *stockFile, *coinsFile;
   int i;
   char dollarStr[CURRENCY_LEN + 1];
   char centStr[CURRENCY_LEN + 1];
   char onHandStr[QTY_LEN + 1];
   char denomStr[DENOM_LEN + 1];
   char qtyStr[QTY_LEN + 1];
   
   Node *currentNode;
   
   currentNode = system->item_list->head;
   
   stockFile = fopen(system->stock_file_name, "wb");
   coinsFile = fopen(system->coin_file_name, "wb");
   
   if (stockFile == NULL || coinsFile == NULL)
   {
      printf("[ERROR] File(s) not found. \n");
      printf("System not saved. Quitting... \n");
      return FALSE;
   }
   
   while (currentNode != NULL)
   {
      sprintf(dollarStr, "%d", currentNode->data->price.dollars);
      sprintf(centStr, "%d", currentNode->data->price.cents);
      sprintf(onHandStr, "%d", currentNode->data->on_hand);
      
      /* id */
      fwrite(currentNode->data->id, strlen(currentNode->data->id), 1, stockFile);
      fwrite("|", sizeof(char), 1, stockFile);
      
      /* name */
      fwrite(currentNode->data->name, strlen(currentNode->data->name), 1, stockFile);
      fwrite("|", sizeof(char), 1, stockFile);
      
      /* description */
      fwrite(currentNode->data->desc, strlen(currentNode->data->desc), 1, stockFile);
      fwrite("|", sizeof(char), 1, stockFile);
      
      /* dollars */
      fwrite(dollarStr, strlen(dollarStr), 1, stockFile);
      fwrite(".", sizeof(char), 1, stockFile);
      
      /* cents */
      fwrite(centStr, strlen(centStr), 1, stockFile);
      fwrite("|", sizeof(char), 1, stockFile);
      
      /* on hand */
      fwrite(onHandStr, strlen(onHandStr), 1, stockFile);
      fwrite("\n", sizeof(char), 1, stockFile);
      
      currentNode = currentNode->next;
   }
   
   for (i = NUM_DENOMS - 1; i >= 0; i--)
   {
      sprintf(denomStr, "%d", system->cash_register[i].denom);
      sprintf(qtyStr, "%d", system->cash_register[i].count);
      
      /* denomination */
      fwrite(denomStr, strlen(denomStr), 1, coinsFile);
      fwrite(",", sizeof(char), 1, coinsFile);
      
      /* quantity */
      fwrite(qtyStr, strlen(qtyStr), 1, coinsFile);
      fwrite("\n", sizeof(char), 1, coinsFile);
   }
   
   fclose(stockFile);
   fclose(coinsFile);
   
   printf("System saved successfully. Quitting... \n");
   
   return FALSE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true when adding an item succeeds and false when it does not
 **/
BOOLEAN add_item(System *system)
{
   Item *newItem = malloc(sizeof(Item));
   BOOLEAN nameChecked = FALSE, descChecked = FALSE, priceChecked = FALSE;
   char *namePrompt = "Enter the item name: ";
   char *descPrompt = "Enter the item description: ";
   char *pricePrompt = "Enter the price for this item: ";
   char price[4+1];
   char *dollars, *cents, *endPtr;
   
   printf("\nAdd Item \n\n");
   
   /* get id */
   if (!generateID(system, newItem->id))
   {
      printf("[ERROR] System has reached maximum capacity, cannot add a new item. \n");
      free(newItem);
      return TRUE;
   }
   printf("This new item will have the item ID of %s \n", newItem->id);
   
   /* get item name */
   while (!nameChecked)
   {
      if (!validInpStr(namePrompt, newItem->name, NAME_LEN))
      {
         free(newItem);
         return TRUE;
      }
      else if (!validName(newItem->name))
      {
         continue;
      }

      nameChecked = TRUE;
   }
   
   /* get item desc */
   while (!descChecked)
   {
      if (!validInpStr(descPrompt, newItem->desc, DESC_LEN))
      {
         free(newItem);
         return TRUE;
      }
      else if (!validDesc(newItem->desc))
      {
         continue;
      }
      
      descChecked = TRUE;
   }
   
   /* get item price */
   while (!priceChecked)
   {
      if (!validInpStr(pricePrompt, price, DESC_LEN))
      {
         free(newItem);
         return TRUE;
      }
      dollars = strtok(price, ".");
      cents = strtok(NULL, ".");
      newItem->price.dollars = strtoul(dollars, &endPtr, 10);
      newItem->price.cents = strtoul(cents, &endPtr, 10);
      
      if (!validPrice(newItem->price))
      {
         continue;
      }
      
      priceChecked = TRUE;
   }
   
   /* set on hand value */
   newItem->on_hand = DEFAULT_STOCK_LEVEL;
   
   if (addToStockList(system, newItem))
   {
      printf("This item “%s – %s” has now been added to the menu. \n\n", newItem->name, newItem->desc);
      free(newItem);
      return TRUE;
   }
   else
   {
      printf("[ERROR] Item could not be added. Quitting... \n");
      free(newItem);
      return FALSE;
   }
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true when removing an item succeeds and false when it does not
 **/
BOOLEAN remove_item(System *system)
{
   Item *selectedItem;
   char *idPrompt = "Enter the item ID of the item to remove from the menu: ";
   
   printf("\nPurchase Item \n\n");
   
   selectedItem = stockSearch(idPrompt, system);
   
   if (selectedItem == NULL)
   {
      return TRUE;
   }
   
   if (removeFromStockList(system, selectedItem->id))
   {
      printf("“%s – %s: %s” has been removed from the system. \n", selectedItem->id, selectedItem->name, selectedItem->desc);
      return TRUE;
   }
   else 
   {
      printf("[ERROR] Item could not be removed. Quitting... \n");
      return FALSE;
   }
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true as this function cannot fail
 **/
BOOLEAN display_coins(System *system)
{
   int i;
   const char *denoms[NUM_DENOMS] =
   {
      "5 cents",  "10 cents", "20 cents", "50 cents",
      "1 dollar", "2 dollars", "5 dollars", "10 dollars"
   };
   
   printf("\nCoins Summary \n\n");
   printf("%-12s | %-5s \n", "Denomination", "Count");
   printf("---------------------\n");
   
   for (i = 0; i < NUM_DENOMS; i++)
   {
      printf("%-12s | ", denoms[i]);
      printf("%-5d \n", system->cash_register[i].count);
   }
   
   printf("\n");
   return TRUE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true as this function cannot fail.
 **/
BOOLEAN reset_stock(System *system)
{
   Node *currentNode;

   currentNode = system->item_list->head;
   
   while (currentNode != NULL) 
   {
      currentNode->data->on_hand = DEFAULT_STOCK_LEVEL;
      currentNode = currentNode->next;
   }
      
   printf("All stock has been reset to the default level of %d \n", DEFAULT_STOCK_LEVEL);
   return TRUE;
}

/**
 * @param system a pointer to a  ppd_system struct that contains 
 * all the information for managing the system.
 * @return true as this function cannot fail.
 **/
BOOLEAN reset_coins(System *system)
{
   int i;
   
   for(i = 0; i < NUM_DENOMS; i++)
   {
      system->cash_register[i].count = DEFAULT_COIN_COUNT;
   }
      
   printf("All coins has been reset to the default level of %d \n", DEFAULT_COIN_COUNT);
   return TRUE;
}

BOOLEAN abort_program(System *system)
{
   printf("Program aborted, all changes lost. Quitting... \n");
   return FALSE;
}

/* ofrmats and prints a given item */
void printItem(System *system, Item *item)
{
   char centStr[CURRENCY_LEN + 1];
   
   sprintf(centStr, "%d", item->price.cents);
   if (strcmp(centStr, "0") == 0)
   {
      strcpy(centStr, "00");
   }
   
   printf("%-*s | %-*s | %-*u | $%*u.%*s \n", ID_COL_LEN, item->id, NAME_COL_LEN(system), item->name, AVAIL_COL_LEN, item->on_hand, CURRENCY_LEN, item->price.dollars, CURRENCY_LEN, centStr);
}

/* determines the size of the name column depending on the biggest name */
int NAME_COL_LEN(System *system)
{
   Node *currentNode = system->item_list->head;
   int nameLength = strlen(currentNode->data->name);
   
   while(currentNode != NULL)
   {
      if (strlen(currentNode->data->name) > nameLength)
      {
         nameLength = strlen(currentNode->data->name);
      }         

      currentNode = currentNode->next;
   }
   
   return nameLength;
}

/* convert an int into a price */
void inpToPrice(struct price *price, int priceInp)
{
   price->dollars = (priceInp / 100);
   price->cents = (priceInp % 100);
}

/* convert a price into a string */
void priceToStr(char *priceStr, struct price price)
{
   char centStr[CURRENCY_LEN + 1];

   if (price.cents == 0)
   {
      strcpy(centStr, "00");
   }
   else if (price.cents == 5)
   {
      strcpy(centStr, "05");
   }
   else
   {
      sprintf(centStr, "%d", price.cents);
   }
   
   sprintf(priceStr, "$%2d.%2s", price.dollars, centStr);
}

/* validates insert amount for purchase */
int validInsertAmt(int min, int max)
{
   BOOLEAN inpChecked = FALSE;
   char input[INSERT_LEN + EXTRA_SPACES];
   char *insertPrompt = "";
   int insertInt;
   
   while (inpChecked == FALSE)
   {
      if (validInpStr(insertPrompt, input, INSERT_LEN))
      {
         insertInt = convStrToInt(input, min, max);
      
         if (insertInt != FALSE)
         {
            inpChecked = TRUE;
         }
      }
   }
   
   return insertInt;
}

/* searches the list for a given id and returns the matching item */
Item *stockSearch(char *prompt, System *system)
{
   BOOLEAN inpChecked = FALSE;
   char searchID[ID_LEN + EXTRA_SPACES];
   Node *currentNode = system->item_list->head;
   
   while (!inpChecked)
   {
      if (validInpStr(prompt, searchID, ID_LEN))
      {
         if (!validID(searchID))
         {
            continue;
         }
         while(currentNode != NULL)
         {
            if (strcmp(currentNode->data->id, searchID) == 0)
            {
               return currentNode->data;
            }
            
            currentNode = currentNode->next;
         }
         
         printf("[INVALID] The ID you have entered doesn't exist. \n");
         currentNode = system->item_list->head;
      }
      else
      {
         return NULL;
      }
   }
   
   return NULL;
}

/* generates the next highest ID number in the list */
BOOLEAN generateID(System *system, char *itemID)
{
   Node *currentNode = system->item_list->head;
   int highestID = convStrToInt(currentNode->data->id+1, MIN_ID_NUM, MAX_ID_NUM); /* +1 is to avoid the 'I' */
   
   while(currentNode != NULL)
   {  
      if (highestID < convStrToInt(currentNode->data->id+1, MIN_ID_NUM, MAX_ID_NUM))
      {
         highestID = convStrToInt(currentNode->data->id+1, MIN_ID_NUM, MAX_ID_NUM);
      }

      currentNode = currentNode->next;
   }
   
   highestID++;
   
   if (highestID > MAX_ID_NUM)
   {
      return FALSE;
   }
   
   sprintf(itemID, "I%04d", highestID);
   
   return TRUE;
}