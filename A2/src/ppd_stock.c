 /***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #2
 * Full Name        : Rei Ito
 * Student Number   : s3607050
 * Course Code      : COSC1076
 * Program Code     : BP215
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/
#include "ppd_stock.h"

/**
 * @file ppd_stock.c this is the file where you will implement the 
 * interface functions for managing the stock list.
**/

/* adds a new item to the linked stock list */
BOOLEAN addToStockList(System *system, Item *item)
{
   Node *previousNode, *currentNode;

   Node *newNode = malloc(sizeof(*newNode));
   if(newNode == NULL)
   {
      return FALSE;
   }
   newNode->data = malloc(sizeof(*newNode->data));
   if(newNode->data == NULL)
   {
      free(newNode);
      return FALSE;
   }

   *newNode->data = *item;
   newNode->next = NULL;

   previousNode = NULL;
   currentNode = system->item_list->head;
   while (currentNode != NULL)
   {
      if (strcmp(item->name, currentNode->data->name) < 0)
      {
         break;
      }

      previousNode = currentNode;
      currentNode = currentNode->next;
   }

   if (system->item_list->head == NULL)
   {
      /* Empty list. */
      system->item_list->head = newNode;
   }
   else if (previousNode == NULL)
   {
      /* Inserting at the head. */
      newNode->next = system->item_list->head;
      system->item_list->head = newNode;
   }
   else
   {
      previousNode->next = newNode;
      newNode->next = currentNode;
   }
   
   system->item_list->count++;

   return TRUE;
}

/* removes an existing item to the linked stock list */
BOOLEAN removeFromStockList(System *system, char *id)
{
   Node *previousNode, *currentNode;

   previousNode = NULL;
   currentNode = system->item_list->head;
   
   while(currentNode != NULL)
   {
      if(strcmp(id, currentNode->data->id) == 0)
      {
         break;
      }

      previousNode = currentNode;
      currentNode = currentNode->next;
   }
   
   if(currentNode == NULL)
   {
      return FALSE;
   }

   if(previousNode == NULL)
   {
      /* Remove at the head. */
      system->item_list->head = currentNode->next;
   }
   else
   {
      previousNode->next = currentNode->next;
   }

   free(currentNode->data);
   free(currentNode);
   
   system->item_list->count--;
   
   return TRUE;
}

/* search to determine if the given ID exists */
BOOLEAN idExists(System *system, char *idStr)
{
   Node *currentNode;
   
   currentNode = system->item_list->head;
   
   while(currentNode != NULL)
   {
      if(strcmp(idStr, currentNode->data->id) == 0)
      {
         printf("[INVALID] ID already exists. \n");
         return TRUE;
      }
      
      currentNode = currentNode->next;
   }
   
   return FALSE;
}

/* validates the given ID */
BOOLEAN validID(char *idStr)
{
   int i, idInt = 0;

   if (strlen(idStr) != ID_LEN)
   {
      printf("[INVALID] ID must have a length of 5 starting with 'I' and the rest being numbers. \n");
      return FALSE;
   }
   
   if (idStr[0] != 'I')
   {
      printf("[INVALID] ID must start with an 'I'. \n");
      return FALSE;
   }
   
   for (i = 1; i < strlen(idStr); i++)
   {
      if (idStr[i] >= '0' && idStr[i] <= '9')
      {
         idInt++;
      }
   }
   
   if (idInt == 4)
   {
      return TRUE;
   }
   else
   {
      printf("[INVALID] ID must have only numbers after 'I'. \n");
      return FALSE;
   }
}

/* validates the given name */
BOOLEAN validName(char *nameStr)
{
   if (strlen(nameStr) > NAME_LEN)
   {
      printf("[INVALID] Name length is too long. \n");
      return FALSE;
   }
   
   return TRUE;
}

/* validates the given description */
BOOLEAN validDesc(char *descStr)
{
   if (strlen(descStr) > DESC_LEN)
   {
      printf("[INVALID] Description length is too long. \n");
      return FALSE;
   }
   
   return TRUE;
}

/* validates the given price */
BOOLEAN validPrice(struct price price)
{
   if (price.dollars > MAX_DOLLARS || price.cents > MAX_CENTS)
   {
      printf("[INVALID] Price is too much. \n");
      return FALSE;
   }
   
   if ((price.cents % 5) != 0)
   {
      printf("[INVALID] Cents isn't valid denomination of money. \n");
      return FALSE;
   }
   
   return TRUE;
}

/* validates the on hand value given */
BOOLEAN validOnHand(char *onHandStr)
{
   if (strlen(onHandStr) > ON_HAND_LEN)
   {
      printf("[INVALID] On hand length is too long. \n");
      return FALSE;
   }
   
   if (strcmp(onHandStr, "0") < 0 || strcmp(onHandStr, "99") > 0)
   {
      printf("[INVALID] On hand is too small/big. \n");
      return FALSE;
   }
   
   return TRUE;
}