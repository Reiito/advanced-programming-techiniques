 /***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #2
 * Full Name        : Rei Ito
 * Student Number   : s3607050
 * Course Code      : COSC1076
 * Program Code     : BP215
 * Start up code provided by Paul Miller
 * Some codes are adopted here with permission by an anonymous author
 ***********************************************************************/

#include "ppd_main.h"
#include "ppd_menu.h"
#include "ppd_options.h"
#include "ppd_utility.h"

/**
 * @file ppd_main.c contains the main function implementation and any 
 * helper functions for main such as a display_usage() function.
 **/

/**
 * manages the running of the program, initialises data structures, loads
 * data and handles the processing of options. The bulk of this function
 * should simply be calling other functions to get the job done.
 **/
int main(int argc, char **argv)
{
   const char *stockFile = argv[1], *coinsFile = argv[2];
   System system;
   struct menu_item menu[NUM_MENU_ITEMS];
   BOOLEAN isRunning = TRUE;
   
   /* validate command line arguments */
   if(argc != NUM_ARGS)
   {
      printf("[ERROR] Arguments are invalid. Quitting... \n");
      return EXIT_FAILURE;
   }
   
   /* initialise the system */
   if (!system_init(&system))
   {
      printf("[ERROR] System could not be initialised. Quitting... \n");
      return EXIT_FAILURE;
   }
   
   /* load data */
   if (!load_stock(&system, stockFile))
   {
      printf("[ERROR] Stock could not be loaded. Quitting... \n");
      system_free(&system);
      return EXIT_FAILURE;
   }
   else if (!load_coins(&system, coinsFile))
   {
      printf("[ERROR] '%s' could not be loaded. Quitting... \n", coinsFile);
      system_free(&system);
      return EXIT_FAILURE;
   }
   
   /* initialise the menu system */
   init_menu(menu);
   
   /* loop, asking for options from the menu */
   while (isRunning)
   {
      if (!get_menu_choice(menu)(&system))
      {
         isRunning = FALSE;
      }
   }
   
   /* frees all memory before the program exits */
   system_free(&system);

   return EXIT_SUCCESS;
}
