/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #1 
 * Full Name        : Rei Ito
 * Student Number   : s3607050
 * Course Code      : COSC1076
 * Program Code     : BP215
 * Start up code provided by Paul Miller 
 **********************************************************************/
 
#include "shared.h"

#ifndef GAMEBOARD_H
#define GAMEBOARD_H

/*forward declaration of the 'player' structure.*/
struct player;

/*Gameboard sizes*/
#define COLUMN_WIDTH 4
#define BOARD_HEIGHT 8
#define BOARD_WIDTH BOARD_HEIGHT

/*Type definition of a game board*/
typedef enum cell game_board[BOARD_HEIGHT][BOARD_WIDTH];

/*Function prototypes*/
void init_game_board (game_board board);
void display_board (game_board board, struct player *first, struct player *second);

#endif /* ifndef GAMEBOARD_H */