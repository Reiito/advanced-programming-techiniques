/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #1 
 * Full Name        : Rei Ito
 * Student Number   : s3607050
 * Course Code      : COSC1076
 * Program Code     : BP215
 * Start up code provided by Paul Miller 
 **********************************************************************/
 
#include "game.h"

/**
This function takes in two player structures in order to execute the game and return a winner.
First both the gameboard and the players are initialized after being checked if the names were valid.
Then the players are assigned to 'current' and 'other' based on their token color.
The next stage was to start a game loop that will call both 'make_move()' and 'apply_move()'
to validate and execute a given move.
Finally if the player has pressed either ctrl-d or enter the game will return the player with the 
highest score (if there is one), else it'll swap the plays and start the next round.
**/
struct player *play_game (struct player *first, struct player *second)
{
	game_board board;
	/*enum cell token;*/
	struct player *current, *other, *winner = NULL;
   BOOLEAN playGame = TRUE;
   char *currColor;
   
   if (!init_first_player(first, &first->token) || !init_second_player(second, first->token))
   {
      return winner;
   }

   printEquals();
   
   if (first->token == RED)
   {
      current = first;
      other = second;
   }
   else
   {
      current = second;
      other = first;
   }
   
   printf("%s goes first.\n", current->name);
   
   init_game_board(board);
   
   while (playGame)
   {
      first->score = game_score(board, first->token);
      second->score = game_score(board, second->token);
      
      display_board(board, first, second);
      
      /*The 'if-else' and 'printf()' are so that the player's name is in their token's color
      This helps with clarity when both testing and playing*/
      if (current->token == RED)
      {
         currColor = COLOR_RED;
      }
      else
      {
         currColor = COLOR_BLUE;
      }
      printf("It is %s%s's%s turn.\n", currColor, current->name, COLOR_RESET);
      
      if (!make_move(current, board))
      {
         if (game_score(board, first->token) == game_score(board, second->token))
         {
            winner = NULL;
            playGame = FALSE;
         }
         else if (game_score(board, first->token) < game_score(board, second->token))
         {
            winner = second;
            playGame = FALSE;
         }
         else
         {
            winner = first;
            playGame = FALSE;
         }
      }
      else
      {
         swap_players(&current, &other);
      }
   }

   return winner;
}

/* Move validation and application: returning a BOOLEAN value for easy conditioning*/
BOOLEAN make_move (struct player *player, game_board board)
{
   BOOLEAN moveValid = FALSE;
   int posX, posY;
   char prompt[LINE_LEN];
   
   while (!moveValid)
   {
      sprintf(prompt, "Please enter x and y coordinates separated by a comma for the piece you wish to place: ");
      
      if (!validMoveInput(prompt, TURN_LEN, &posX, &posY, TURN_MIN, TURN_MAX))
      {
         return FALSE;
      }
      else if (!apply_move(board, posX-1, posY-1, player->token))
      {
         printf("[INVALID] The wrong move has been made.\n");
      }
      else
      {
         moveValid = TRUE;
      }
   }
   
   return TRUE;
}

/*Move application validation.*/
BOOLEAN apply_move (game_board board, int x, int y, enum cell player_token)
{  
   enum direction dir;
   enum cell currToken, oppToken;
   unsigned captured_pieces = 0, directionsChecked = 0;
   int yCheck, xCheck, yFlip = y, xFlip = x;
   
   yFlip = y;
   xFlip = x;

   if (player_token == RED)
   {
      currToken = player_token;
      oppToken = BLUE;
   }
   else 
   {
      currToken = player_token;
      oppToken = RED;
   }
   
   for (dir = 0; dir < DIRECTIONS; dir++)
   {
      switch (dir)
      {
         case NORTH:
            for (yCheck = y; yCheck >= 0; yCheck--)
            {
               if (board[yCheck][x] != BLANK)
               {
                  if (board[yCheck][x] == oppToken)
                  {
                     captured_pieces++;
                  }
                  else if (board[yCheck][x] == currToken && captured_pieces > 0)
                  {
                     for (; yCheck >= captured_pieces; yCheck--)
                     {
                        board[yFlip--][x] = currToken;
                     }
                     captured_pieces = 0;
                     directionsChecked++;
                  }
               }
               else
               {
                  continue;
               }
            }
            break;
            
         case SOUTH:
            for (yCheck = y; yCheck < BOARD_WIDTH; yCheck++)
            {
               if (board[yCheck][x] != BLANK)
               {
                  if (board[yCheck][x] == oppToken)
                  {
                     captured_pieces++;
                  }
                  else if (board[yCheck][x] == currToken && captured_pieces > 0)
                  {
                     for (; yCheck >= captured_pieces; yCheck--)
                     {
                        board[yFlip++][x] = currToken;
                        captured_pieces--;
                     }
                     directionsChecked++;
                  }
               }
               else
               {
                  continue;
               }
            }
            break;
            
         case EAST:
            for (xCheck = x; xCheck < BOARD_WIDTH; xCheck++)
            {
               if (board[y][xCheck] != BLANK)
               {
                  if (board[y][xCheck] == oppToken)
                  {
                     captured_pieces++;
                  }
                  else if (board[y][xCheck] == currToken && captured_pieces > 0)
                  {
                     for (; xCheck >= captured_pieces; xCheck--)
                     {
                        board[y][xFlip++] = currToken;
                     }
                     captured_pieces = 0;
                     directionsChecked++;
                  }
               }
               else
               {
                  continue;
               }
            }
            break;
            
         case WEST:
            for (xCheck = x; xCheck >= 0; xCheck--)
            {
               if (board[y][xCheck] != BLANK)
               {
                  if (board[y][xCheck] == oppToken)
                  {
                     captured_pieces++;
                  }
                  else if (board[y][xCheck] == currToken && captured_pieces > 0)
                  {
                     for (; xCheck >= captured_pieces; xCheck--)
                     {
                        board[y][xFlip--] = currToken;
                     }
                     captured_pieces = 0;
                     directionsChecked++;
                  }
               }
               else
               {
                  continue;
               }
            }
            break;
            
         case NORTH_EAST:
            for (yCheck = y, xCheck = x; yCheck >= 0 && xCheck < BOARD_WIDTH; yCheck--, xCheck++)
            {
               if (board[yCheck][xCheck] != BLANK)
               {
                  if (board[yCheck][xCheck] == oppToken)
                  {
                     captured_pieces++;
                  }
                  else if (board[yCheck][xCheck] == currToken && captured_pieces > 0)
                  {
                     for (; yCheck >= captured_pieces && xCheck >= captured_pieces; yCheck--, xCheck--)
                     {
                        board[yFlip--][xFlip++] = currToken;
                     }
                     captured_pieces = 0;
                     directionsChecked++;
                  }
               }
               else
               {
                  continue;
               }
            }
            break;
            
         case NORTH_WEST:
            for (yCheck = y, xCheck = x; yCheck >= 0 && xCheck >= 0; yCheck--, xCheck--)
            {
               if (board[yCheck][xCheck] != BLANK)
               {
                  if (board[yCheck][xCheck] == oppToken)
                  {
                     captured_pieces++;
                  }
                  else if (board[yCheck][xCheck] == currToken && captured_pieces > 0)
                  {
                     for (; yCheck >= captured_pieces && xCheck >= captured_pieces; yCheck--, xCheck--)
                     {
                        board[yFlip--][xFlip--] = currToken;
                     }
                     captured_pieces = 0;
                     directionsChecked++;
                  }
               }
               else
               {
                  continue;
               }
            }
            break;
         
         case SOUTH_EAST:
            for (yCheck = y, xCheck = x; yCheck < BOARD_HEIGHT && xCheck < BOARD_WIDTH; yCheck++, xCheck++)
            {
               if (board[yCheck][xCheck] != BLANK)
               {
                  if (board[yCheck][xCheck] == oppToken)
                  {
                     captured_pieces++;
                  }
                  else if (board[yCheck][xCheck] == currToken && captured_pieces > 0)
                  {
                     for (; yCheck >= captured_pieces && xCheck >= captured_pieces; yCheck--, xCheck--)
                     {
                        board[yFlip++][xFlip++] = currToken;
                     }
                     captured_pieces = 0;
                     directionsChecked++;
                  }
               }
               else
               {
                  continue;
               }
            }
            break;
         
         case SOUTH_WEST:
            for (yCheck = y, xCheck = x; yCheck < BOARD_HEIGHT && xCheck >= 0; yCheck++, xCheck--)
            {
               if (board[yCheck][xCheck] != BLANK)
               {
                  if (board[yCheck][xCheck] == oppToken)
                  {
                     captured_pieces++;
                  }
                  else if (board[yCheck][xCheck] == currToken && captured_pieces > 0)
                  {
                     for (; yCheck >= captured_pieces && xCheck >= captured_pieces; yCheck--, xCheck--)
                     {
                        board[yFlip++][xFlip--] = currToken;
                     }
                     captured_pieces = 0;
                     directionsChecked++;
                  }
               }
               else
               {
                  continue;
               }
            }
            break;
      }
   }
   
   if (directionsChecked > 0)
   {
      return TRUE;
   }
   else
   {
      return FALSE;
   }
}

/*Counts and returns the total of all the tokens on the board based on the parameter token color.*/
unsigned game_score (game_board board, enum cell player_token)
{
   int row, col;
   unsigned score = 0;
   
   for (row = 0; row < BOARD_HEIGHT; row++)
   {
      for (col = 0; col < BOARD_WIDTH; col++)
      {
         if (board[row][col] == player_token)
         {
            score++;
         }
      }
   }
   
   return score;
}

/*Swaps the players around in memory.*/
void swap_players (struct player **first, struct player **second)
{
   struct player tempPlayer;
   
   tempPlayer = **first;
   **first = **second;
   **second = tempPlayer;
}