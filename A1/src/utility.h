/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #1 
 * Full Name        : Rei Ito
 * Student Number   : s3607050
 * Course Code      : COSC1076
 * Program Code     : BP215
 * Start up code provided by Paul Miller 
 **********************************************************************/
 
#include "shared.h"
#include <limits.h>

#ifndef UTILITY_H
#define UTILITY_H

/*The total width of the entire board.*/
#define TOTAL_WIDTH 9

/*Default line length.*/
#define LINE_LEN 80

/*The last two characters required in a string.*/
#define NEW_LINE_SPACE 1
#define NULL_SPACE 1
#define EXTRA_SPACES (NEW_LINE_SPACE + NULL_SPACE)

/*Newline character*/
#define NEW_LINE '\n'

/*Function prototypes*/
void read_rest_of_line (void);
void printEquals (void);
void printDashes (void);
void displayMenu (void);
int convStrToInt (char *string, char *inputType, int min, int max);
BOOLEAN validInpStr (char *prompt, char *usrString, unsigned strInpLen);
int validMenuInt (char *prompt, unsigned intInpLen, int min, int max);
BOOLEAN validMoveInput (char *prompt, unsigned intInpLen, int *xValue, int *yValue, int min, int max);

#endif /* ifndef UTILITY_H */
