/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #1 
 * Full Name        : Rei Ito
 * Student Number   : s3607050
 * Course Code      : COSC1076
 * Program Code     : BP215
 * Start up code provided by Paul Miller 
 **********************************************************************/
 
#include "player.h"
#include "game.h"

/*Print player details*/
void printPlayer (struct player player, int playerOrder)
{
   char *color;
   
   if (player.token == RED)
   {
      color = COLOR_RED;
   }
   else
   {
      color = COLOR_BLUE;
   }
   
   printf("Player %d's Details:\n", playerOrder);
   printDashes();
   printf("Name: %-20s\n", player.name);
   printf("Score: %-2d\n", player.score);
   printf("Token Color: %s0%s\n", color, COLOR_RESET);
   printEquals();
}

/*Prompts and checks for a valid player name to return true, else it'll return false.
Also randomises the player's token to be between 1 and 2 (RED or BLUE) and setting thier score to 0.*/
BOOLEAN init_first_player (struct player *first, enum cell *token)
{
   char name[NAME_LEN];
   char prompt[LINE_LEN];
   srand(time(NULL));
   
   sprintf(prompt, "Please enter player %d's name (Maximum of %d characters): ", PLAYER_ONE, NAME_LEN);
   if (!validInpStr(prompt, name, NAME_LEN))
   {
      return FALSE;
   }
   
   strcpy(first->name, name);
   first->token = rand() % NUM_COLORS + 1;
   first->score = 0;
   
   return TRUE;
}

/*Very similar to 'init_first_player' but takes the first player's token instead
and makes the second player's token equal the opposite color.*/
BOOLEAN init_second_player (struct player *second, enum cell token)
{
   char name[NAME_LEN];
   char prompt[LINE_LEN+1];
   
   sprintf(prompt, "Please enter player %d's name (Maximum of %d characters): ", PLAYER_TWO, NAME_LEN);
   if (!validInpStr(prompt, name, NAME_LEN))
   {
      return FALSE;
   }
   
   strcpy(second->name, name);
   
   if (token == BLUE)
   {
      second->token = RED;
   }
   else
   {
      second->token = BLUE;
   }
   
   second->score = 0;
   
   return TRUE;
}