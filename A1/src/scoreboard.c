/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #1 
 * Full Name        : Rei Ito
 * Student Number   : s3607050
 * Course Code      : COSC1076
 * Program Code     : BP215
 * Start up code provided by Paul Miller 
 **********************************************************************/

#include "scoreboard.h"

/*Initialises all the scores to be empty.*/
void init_scoreboard (score scores[MAX_SCORES])
{
   int i;
   
   for (i = 0; i <= MAX_SCORES; i++)
   {
      strcpy(scores[i].name, " ");
      scores[i].score = 0;
   }
}

/*The first loop goes through the scores array and fills in the empty spots with the winner 
but if the scoreboard is full, it'll only put the winner in if the winner has a higher score than last place.
Then there's the second part of the function which sorts all items in the array from highest to smallest.*/
BOOLEAN add_to_scoreboard (score scores[MAX_SCORES], struct player *winner)
{
   int i, j;
   struct player swapPlayerTemp;
   
   for (i = 0; i < MAX_SCORES; i++)
   {
      if (scores[i].score == 0)
      {
         scores[i] = *winner;
         break;
      }
      if (i == MAX_SCORES - 1 && scores[i].score < winner->score)
      {
         scores[i] = *winner;
      }
   }
   
   for (i = 0; i < MAX_SCORES; i++)
   {
      for (j = i + 1; j < MAX_SCORES; j++)
      {
         if (scores[i].score < scores[j].score)
         {
            swapPlayerTemp = scores[i];
            scores[i] = scores[j];
            scores[j] = swapPlayerTemp;
         }
      }   
   }
   
   return TRUE;
}

/*This function goes through the scoreboard array and displays the details of any player that's not empty.*/
void display_scores (score scores[MAX_SCORES])
{
   int i;
   
   printEquals();
   printf("Reversi - Top Scores\n");
   printDashes();
   printf("%-20s | Score\n", "Name");
   printDashes();
   for (i = 0; i < MAX_SCORES; i++)
   {
      if (strcmp(scores[i].name, " ") != 0)
      {
         printf("%-20s | %d\n", scores[i].name, scores[i].score);
         printDashes();
      }
   }
}