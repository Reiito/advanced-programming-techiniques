/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #1 
 * Full Name        : Rei Ito
 * Student Number   : s3607050
 * Course Code      : COSC1076
 * Program Code     : BP215
 * Start up code provided by Paul Miller 
 **********************************************************************/

#include "utility.h"

/*Buffer clear if the input is out of the array bounds.*/
void read_rest_of_line(void)
{
    int ch;
    while(ch = getc(stdin), ch != EOF && ch != NEW_LINE)
        ;
    clearerr(stdin);
}

/*Prints equals the size of the board.*/
void printEquals (void)
{
   int i;
   
   for (i = 0; i < TOTAL_WIDTH; i++)
   {
      printf("%s", "====");
   }
   printf("\n");
}

/*Prints dashes the size of the board.*/
void printDashes (void)
{
   int i;
   
   for (i = 0; i < TOTAL_WIDTH; i++)
   {
      printf("%s", "----");
   }
   printf("\n");
}

/*Prints the menu options.*/
void displayMenu(void)
{
   printEquals();
   printf("Welcome to Reversi!      \n");
   printEquals();
   printf("Select an option:        \n");       
   printf("1. Play a game           \n");
   printf("2. Display high scores   \n");
   printf("3. Quit the program      \n");
}

/*Takes in a string, input type name and both minimum and maximum values.
It then conversts the string into an integer and check if it's valid.
If it is then it'll return the converted value less, it'll return 'FALSE' (which is just 0).*/
int convStrToInt (char *string, char *inputType, int min, int max)
{
   int integer;
   char *stringEndPtr;
   
   integer = (int)strtol(string, &stringEndPtr, 10);
   if (strcmp(stringEndPtr, "") != 0)
   {
      printf("[INVALID] '%s' Input must be numeric.\n", inputType);
      return FALSE;
   }
   else if (integer < min || integer > max)
   {
      printf("[INVALID] '%s' Input must be between: %d and %d.\n", inputType, min, max);
      return FALSE;
   }
   
   return integer;
}

/*This takes in a prompt, string and the desired string length.
A loop is then implemented and the first thing that's checked is if the user has either 
pressed 'enter' or 'ctrl-d', if so then it'll return false. If not it'll go onto validating 
the string by checking if it is larger than the intented size, clears the buffer and re-prompts the user.
Else it'll return true.*/
BOOLEAN validInpStr (char *prompt, char *usrString, unsigned strInpLen)
{
   BOOLEAN inpChecked = FALSE;
   char input[LINE_LEN + EXTRA_SPACES];
   int inpLen;
   char *usrInp;
   
   while (!inpChecked)
   {
      printf("%s", prompt);
      
      usrInp = fgets(input, strInpLen + EXTRA_SPACES, stdin);
      inpLen = strlen(input);
   
      if (usrInp == NULL)
      {
         printf("\n");
         return FALSE;
      }
      if (strcmp(usrInp, "\n") == 0)
      {
         input[inpLen - 1] = '\0';
         return FALSE;
      }
      
      if(input[inpLen - 1] != '\n')
      {
         printf("[INVALID] Input was too long.\n");
         read_rest_of_line();
      }
      else
      {
         inpChecked = TRUE;
      }
   }
   input[inpLen - 1] = '\0';
   strcpy(usrString, input);
   return TRUE;
}

/*Gets a string and check if it's valid, if not; return false.
Then get converts it into an integer and the same check is conducted
When both return true and are valid the function returns true as well.*/
int validMenuInt (char *prompt, unsigned intInpLen, int min, int max)
{
   BOOLEAN inpChecked = FALSE;
   char input[LINE_LEN + EXTRA_SPACES];
   int menuInt;
   
   while (inpChecked == FALSE)
   {
      if (!validInpStr(prompt, input, intInpLen))
      {
         return FALSE;
      }
      
      menuInt = convStrToInt(input, "Menu", min, max);
      
      if (menuInt != FALSE)
      {
         inpChecked = TRUE;
      }
   }
   
   return menuInt;
}

/*Almost the same as 'validMenuInt' but instead this tokenises the move and converts the two given strings.
It also prevent the user from corrupting the input with commas by checking if the first and last elements of the input are commas.*/
BOOLEAN validMoveInput (char *prompt, unsigned intInpLen, int *xValue, int *yValue, int min, int max)
{
   int x, y;
   BOOLEAN inpChecked = FALSE;
   char input[LINE_LEN + EXTRA_SPACES];
   char *xToken, *yToken;
   
   while (!inpChecked)
   {
      if (!validInpStr(prompt, input, intInpLen))
      {
         return FALSE;
      }
      
      if (input[0] == ',' || strcmp(&input[intInpLen-1], ",") == 0)
      {
         printf("[INVALID] The 'x' and 'y' values cannot be commas.\n");
         continue;
      }
      
      xToken = strtok(input, ",");
      yToken = strtok(NULL, ",");
      
      x = convStrToInt(xToken, "x", min, max);
      y = convStrToInt(yToken, "y", min, max);
      
      if (x != FALSE && y != FALSE)
      {
         inpChecked = TRUE;
      }
   }
   
   *xValue = x;
   *yValue = y;
   return TRUE;
}