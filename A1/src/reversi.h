/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #1 
 * Full Name        : Rei Ito
 * Student Number   : s3607050
 * Course Code      : COSC1076
 * Program Code     : BP215
 * Start up code provided by Paul Miller 
 **********************************************************************/
#include "shared.h"
#include "player.h"
#include "game.h"
#include "scoreboard.h"
#include "utility.h"

#ifndef REVERSI_H
#define REVERSI_H

/*Menu input variables*/
#define CHOICE_LEN 1
#define MENU_MIN 1
#define MENU_MAX 3

#endif