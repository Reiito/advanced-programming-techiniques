/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #1 
 * Full Name        : Rei Ito
 * Student Number   : s3607050
 * Course Code      : COSC1076
 * Program Code     : BP215
 * Start up code provided by Paul Miller 
 **********************************************************************/

#include "reversi.h"

/*The main function that executes functions from all the other files.
First the variables are defined and the scoreboard initialized, then goes into the menu loop.
It displays the menu, gets a valid input and depending on the value, a action is executed.
1: Calls the play game function and depending on what 'winner' equals; it can end in a draw
or a winner is found and is added to the score board
2: Displays the ordered scores in a board.
3: Exits the game.*/
int main(void)
{
   score scoreBoard[MAX_SCORES];
   struct player first, second, *winner = NULL;
   int usrChoiceInt;
   BOOLEAN menuExit = FALSE;
   char prompt[LINE_LEN];
   
   init_scoreboard(scoreBoard);

   while (!menuExit)
   {
      displayMenu();
      
      sprintf(prompt, "Please enter your choice: ");
      usrChoiceInt = validMenuInt(prompt, CHOICE_LEN, MENU_MIN, MENU_MAX);
      
      if (usrChoiceInt == FALSE)
      {
         continue;
      }

      switch (usrChoiceInt)
      {
         case 1:
            winner = play_game(&first, &second);
            if (winner == NULL)
            {
               printf("There is no winner!\n");
               break;
            }
            else
            {
               if (add_to_scoreboard(scoreBoard, winner))
               {
                  printf("The winner is: %s!\n", winner->name);
                  break;
               }
            }
            break;
         case 2:
            display_scores(scoreBoard);
            break;
         default:
            menuExit = TRUE;
            break;
      }
   }      
   return EXIT_SUCCESS;
}