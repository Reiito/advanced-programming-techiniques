/***********************************************************************
 * COSC1076 - Advanced Programming Techniques
 * Semester 2 2016 Assignment #1 
 * Full Name        : Rei Ito
 * Student Number   : s3607050
 * Course Code      : COSC1076
 * Program Code     : BP215
 * Start up code provided by Paul Miller 
 **********************************************************************/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#include <math.h>
#include <time.h>

#ifndef SHARED_H
#define SHARED_H

/*Definition of the boolean type*/
typedef enum
{
        FALSE, TRUE
} BOOLEAN;

/*The number of colors*/
#define NUM_COLORS 2

/*The three values that'll be stored in the gameboard.*/
enum cell
{
        BLANK, RED, BLUE
};

/*Color codes required to display the tokens on the board.*/
#define COLOR_RED     "\33[31m"
#define COLOR_BLUE    "\33[34m"
#define COLOR_RESET   "\33[0m"
#endif /* defined SHARED_H */